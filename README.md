# HashiCorp Vault Integration with Jenkins Using App Role

- [HashiCorp Vault Integration with Jenkins Using App Role](#hashicorp-vault-integration-with-jenkins-using-app-role)
- [Introduction](#introduction)
- [Prerequisites](#prerequisites)
- [Environment Installation](#environment-installation)
    - [Helm Charts & Vault Configuration Setup](#helm-chrats-&-vault-configuration-setup)
    - [Working on your local machine](#working-on-your-local-machine)
- [Configure Vault](#configure-vault)
- [Configure Jenkins](#configure-jenkins)
- [Setup Sample Application](#setup-sample-application)
- [Setup The Jenkins Pipeline](#setup-the-jenkins-pipeline)
- [End Result](#end-result)
- [Tips And Tricks](#tips-and-tricks)

## Introduction
This example will show how to use app role secret engine in order to inegrate between Vault and Jenkins.
We will show that we can automatically revoke the access after x number of accesses using AppRole by pre configuration of AppRole.  

## Prerequisites
Minikube  
Helm  
kubectl  
vault-cli  

## Environment Installation
#### Helm Charts & Vault Configuration Setup
In order to setup the testing environment we're using minikube [local kubernetes cluster] -  
Please run infra.sh under script folder using this command:  
sh infra.sh  
This command will install minikube, vault & jenkins helm charts.  

#### Working on your local machine
In order to access both Jenkins & Vault we will need to perform some actions:   
Export Vault environment variables -   
export VAULT_ADDR=http://127.0.0.1:8200  
export VAULT_TOKEN=root  

Use Kubernetes to forward the traffic to your localhost -  
kubectl port-forward vault-0 8200   --- use localhost:8200 to access Vault.  
kubectl port-forward svc/jenkins 8080 --- use localhost:8080 to access Jenkins.

## Configure Vault
In order to configure Vault -  
Please run config.sh under script folder using this command:  
sh config.sh  
This command will configure Vault.  
role-id & secret-id will be saved to .json files.  
  
Write Secrets to Vault:  
Please change the secret-id to the nginx secret-id inside script/secret.sh.  
Run the following:  
sh secret.sh  
  
## Configure Jenkins
##### Configure Vault Plugin
Note: K/V Version should always be set to 1 during this example.  
Login to Jenkins using localhost:8080.  
Admin is the default user, Password is defined by you in helm-chars/jenkins-values.yaml  
Install HashiCorp Vault Plugin.     
Configure Vault Plugin -  
Manage-Jenkins ---> Configure System ---> Navigate to Vault Plugin Area --->   
Vault Url = http://vault.default.svc.cluster.local:8200  
  
Create new Credentials Provider -  
Kind = Vault AppRole Credentials.  
Copy Role-ID and Secret-ID from secret-id-jenkins.json & role-id-jenkins.json files and paste it to the fields.  
Path should stay as default (approle).  
ID = vault-jenkins. (or whatever you like)  
  
In The Advanced Tab --->  
Check Skip SSL Verification.  
Change KV Version to 1.  

##### Configure Read Secret
Manage Jenkins --> Manage Credentials --> New Credentials --> Kind = Vault Secret Text Credential,  
Path = secrets/secret, Key = secret-id, ID = secret-id.  

## Set up the Jenkins Pipeline 
Copy the content of pipeline.jenkinsfile.  
On Jenkins UI:  
New Item --> Pipeline --> Configure.  
And Paste the code to the pipeline field.  
  
The Sample pipeline will set up a jenkins agent pod, with kubectl installed and configured, which will then use  
the SECRET_ID environment variable (which Jenkins reads from Vault) and will create a kubernetes secret that the pod can read.  
  
Before Proceeding to next step, Please run the pipeline and verify that the secret generated on the cluster.  
kubectl get secrets | grep secret-id
  
## Setup Sample Application
Build the docker image with the embedded role-id environment variable using this command:  
cd app/docker  
eval $(minikube docker-env)  - to setup minikube as the docker registry.  
Change the Role-id specified in the Dockerfile to the relevant one from role-id-nginx.json file.   
docker build -t app-name:version .  
for example:  
docker build -t yarden-test:v1 .  
the dot tells docker to use a local Dockerfile.  
  
In order to deploy the application:  
cd app/deployment  
kubectl apply -f deploy.yaml  
Please make sure to change the docker image to the relevant name.  
  
Connect to the pod using this command:  
kubectl exec app -it -- /bin/bash
And run the approle.sh script on the pod.  

## End Result 
Application will display role-id, secret-id and a secret value which retrieved from vault using AppRole.  
Run the following in order to access the app:  
kubectl port-forward app 9080  
Access http://localhost:9080 using your browser and view the result.  

## Tips & Tricks
In order to use minikube as a local Docker Registry run the following:  
eval $(minikube docker-env)