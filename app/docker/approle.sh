# Generate payload.json file to hold creds
cat <<EOF > payload.json 
{ 
    "role_id": "$ROLE_ID", 
    "secret_id": "$SECRETID" 
} 
EOF

#Login using Approle and output the response to client-id.txt
curl \
    --request POST \
    --data @payload.json \
    http://vault.default.svc.cluster.local:8200/v1/auth/approle/login | jq ".auth.client_token" >> client-id.txt

# Export the token without the quotes to an environment variable
export CLIENT_TOKEN=$(cat client-id.txt | sed 's/\"//g')

# Read Secret Password
export SECRET_PASSWORD=$(curl \
    -H "X-Vault-Token: $CLIENT_TOKEN" \
    -X GET \
    http://vault.default.svc.cluster.local:8200/v1/secrets/creds | jq ".data.password")

# Read Secret Username
export SECRET_USER=$(curl \
    -H "X-Vault-Token: $CLIENT_TOKEN" \
    -X GET \
    http://vault.default.svc.cluster.local:8200/v1/secrets/creds | jq ".data.username")

# Copy the answers to index.html
echo "<h2>This is the SECRET-ID- $SECRETID</h2>" >> /usr/share/nginx/html/index.html
# Works
echo "<h2>This is the secrets password retrieved from vault - $SECRET_PASSWORD</h2>" >> /usr/share/nginx/html/index.html
# Should return null since the access revokes after 1 try
echo "<h2>This is the secrets username retrieved from vault - $SECRET_USER</h2>" >> /usr/share/nginx/html/index.html




