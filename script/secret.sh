#Create a secret that holds nginx secret-id - jenkins will deliver it to the app
vault write secrets/secret secret-id=b533e1a9-b485-42fc-f79c-b6d301f989d2 # Copy your secret id from secret-id-nginx.json

#Create a secret that the app will read using approle engine
vault write secrets/creds username="asd1" password="asd1" # Copy your secret id from secret-id-nginx.json