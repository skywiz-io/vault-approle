#Check connectivity to vault
vault login root

#Configure Vault
vault auth enable approle

#Configure create role for jenkins
vault write auth/approle/role/jenkins-role token_num_uses=0 secret_id=0 policies="jenkins"

#Get Role-Id
vault read auth/approle/role/jenkins-role/role-id >> role-id-jenkins.json

#Get Secret-Id
vault write -f auth/approle/role/jenkins-role/secret-id >> secret-id-jenkins.json

#Enable KV secret engine to path /secrets
vault secrets enable -path=secrets kv

#Write a policy for jenkins to vault
vault policy write jenkins jenkins.hcl

##############
#Create Role-Id and Secret-Id for Application
#Configure create role for application
#token_num_uses & secret=id defines the number of accesses for this particular role. for infinite access you can set them to 0.
vault write auth/approle/role/nginx-role token_num_uses=1 secret_id=1 policies="nginx"

#Get Role-Id
vault read auth/approle/role/nginx-role/role-id >> role-id-nginx.json

#Get Secret-Id
vault write -f auth/approle/role/nginx-role/secret-id >> secret-id-nginx.json

#Write a policy for jenkins to vault
vault policy write nginx nginx.hcl

