#Set up Minikube 
minikube start --kubernetes-version=1.21.0

#Set up Jenkins
helm install jenkins jenkins/jenkins -f jenkins-values.yaml

#Set up Vault in dev-mode and raft integrated storage
helm install vault hashicorp/vault -f vault-values.yaml

#Apply Access to use kubectl internally
kubectl apply -f sa.yaml